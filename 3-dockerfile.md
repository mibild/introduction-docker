# Dockerfile Basics

## BUILD a Git Client Container

Create a Git Container manually:

```bash
docker run -it --name git alpine sh
  apk --update add git
  git version
  exit
docker commit git docker-git
docker rm git
docker run --rm -it docker-git git version
docker rmi docker-git
```

* **--name**: Assign a name to the container
* **commit**: Create a new image from a container's changes
* **rm**: Remove one or more containers
* **rmi**: Remove one or more images
* **--rm**: Automatically remove the container when it exits

Create a Git Container with Dockerfile:

```bash
cd code/docker-git
docker build -t docker-git .
docker run -it docker-git git version
```

* **build**: Build an image from a Dockerfile

[code/docker-git/Dockerfile](../../code/docker-git/Dockerfile)

```bash
FROM alpine:3.3
RUN apk update
RUN apk add git
```

* The **FROM** instruction sets the Base Image for subsequent instructions
* The **RUN** instruction will execute any commands in a new layer on top of the current image and commit the results

### BUILD an Apache Server Container

Create an Apache Server Container with Dockerfile:

```bash
cd code/docker-apache2
docker build -t docker-apache2 .
docker run -d -p 4003:80 docker-apache2
```

On Linux:

```bash
google-chrome localhost:4003
```

On Mac:

```bash
open "http://$(docker-machine ip default):4003"
```

[code/docker-apache2/Dockerfile](../../code/docker-apache2/Dockerfile)

```bash
FROM alpine:3.3
RUN apk --update add apache2 && rm -rf /var/cache/apk/*
RUN mkdir -p /run/apache2
EXPOSE 80
CMD httpd -D FOREGROUND
```

* The **EXPOSE** instructions informs Docker that the container will listen on the specified network ports at runtime
* The **CMD** instruction sets the command to be executed when running the image

### BUILD a Static website Image

```bash
cd code/hello-world
docker build -t hello-world .
docker run -d --name hello -P hello-world
```

On Linux:

```bash
google-chrome $(docker port hello 80)
```

On Mac:

```bash
open "http://$(docker-machine ip default):${$(docker port hello 80)##*:}"
```

* **-P**: Publish all exposed ports to the host interfaces
* **port**: Lookup the public-facing port that is NAT-ed to PRIVATE_PORT

[code/hello-world/Dockerfile](../../code/hello-world/Dockerfile)

```bash
FROM nginx:1.8-alpine
ADD site /usr/share/nginx/html
```

* The **ADD** instruction will copy new files from <src> and add them to the container's filesystem at path <dest>

### PUSH Image to a Registry

For this step, we'll need to launch a registry:

```bash
docker run -d -p 5000:5000 --name registry registry:2
```

Then tag your image under the registry namespace and push it there:

```bash
REGISTRY=localhost:5000
docker tag hello-world $REGISTRY/$(whoami)/hello-world
docker push $REGISTRY/$(whoami)/hello-world
```

* **tag**: Tag an image into a repository
* **push**: Push an image or a repository to a Docker registry server

### PULL Image from a Repository

```bash
docker pull $REGISTRY/$(whoami)/hello-world
docker run -d -P --name=registry-hello $REGISTRY/$(whoami)/hello-world
```

On Linux:

```bash
google-chrome $(docker port registry-hello 80)
```

On Mac:

```bash
open "http://$(docker-machine ip default):${$(docker port registry-hello 80)##*:}"
```

* **pull**: Pull an image or a repository from a Docker registry server
