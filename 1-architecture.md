# Basics & Architecture

![Architecture](architecture.jpg)

## DAEMON

* Uses an "Execution Driver" `runc` to create Container
* Isolates Kernel-Features
  * cgroups for resource management
  * namespaces for container isolation
  * Union File System (UFS) for image file layers
* Server via tcp://, unix://, http://, https://

### CLIENT

* `docker`, `docker-machine` and every Docker-API client
* Consumer via tcp://, unix://, http://, https://
* Supports different __drivers__ (VirtualBox, Digital Ocean, AWS, Azure, etc.)

### REGISTRY

* [Public Registry](https://hub.docker.com)
* Private Registry

### IMAGES

* Images are Layers `docker history tutum/hello-world:latest`
* Every layer is (naive) cached (is in cache AND same command in stack)

### CONTAINER

* Run from [Base Images](https://hub.docker.com)
* Build from __Dockerfile__ or __docker-compose.yml__

### TOOLS

* Kitematic - UI for Windows and OSX
* Machine - Manage Container locally or remote
* Compose - Build and Run multiple depended Containers
* Swarm - Cluster and Container Orchestration
* Registry2 - On-Premise Image Registry
* Docker Trusted Registry - On-Premise Image Registry Solution with commercial support
* Docker Universal Plane - On-Premise Docker Management Solution with commercial support
* Docker Cloud - Cloud-based Docker Management Solution for multiple Providers and On-Premise Hosts
