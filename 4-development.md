# Docker-Container Developer-Introductions

## Rules of [12-factor-apps](https://12factor.net)

* You ship it - you run it
* Monitoring is the new Testing
* Don't repair - rebuild
* Automate work processes
* Test/Stage/Prod is always the same - create different versions
* Be immutable - create different versions
* Prefer a stateless (programming) model
* Support SIGTERM for graceful shutdowns
* Configuration via Environment-Vars
* Scaling, Logging, Tracing isn't your business

### Using the Docker-API with NodeJS

```bash
npm i dockerode --save
```

```javascript
const Dockerode = require('dockerode');
const localDocker = new Dockerode('unix://var/run/docker.sock');
```

```bash
docker build -t docker-monitor .
docker run -v /var/run/docker.sock:/var/run/docker.sock --privileged docker-monitor
```
